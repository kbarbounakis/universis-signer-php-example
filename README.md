# universis-signer php example

This project demonstrates the usage of [universis-signer](https://gitlab.com/universis/universis-signer.git) services in a php project.

# Start signer as standalone application

If you don't have universis-signer installed download and extract a bundle relative to your operating system from [universis-signer releases](https://gitlab.com/universis/universis-signer/-/releases)

Download a PKCS12 for testing from [https://gitlab.com/universis/universis-signer/-/raw/master/src/test/resources/keystore.p12](https://gitlab.com/universis/universis-signer/-/raw/master/src/test/resources/keystore.p12) and move it inside universis-signer directory.

Execute the following command to start universis signer:

    ./jre/bin/java -jar ./universis-signer.jar -keyStore ./keystore.p12 -storeType PKCS12

or 

    .\jre\bin\java -jar .\universis-signer.jar -keyStore .\keystore.p12 -storeType PKCS12

if you are in windows environment.

# Execute tests

This project contains some PHPUnit tests for using universis-signer in a PHP project.


Install project dependencies:

    composer install

and run tests:

    ./vendor/bin/phpunit tests/SignerTest.php

## Get certificates list

```
    public function testGetCertificates(): void
    {
        $client = new Client();
        $response = $client->request('GET', 'http://localhost:2465/keystore/certs/', [
            // set basic authorization
            'auth' => ['user', 'secret'] 
        ]);
        // get certificate
        $certs = json_decode($response->getBody()) ;
        $this->assertNotNull($certs);
        var_dump($certs);
        $this->assertEquals($certs[0]->expired, false);
        $this->assertEquals($certs[0]->thumbprint, 'd33ac6cc6290bcfb4a23243af0ec98b4f49b2238');
    }
```

## Sing a pdf document

```
    public function testSignDocument(): void
    {
        $dest = __DIR__ . '/lorem-ipsum-signed.pdf';
        $client = new Client();
        $response = $client->request('POST', 'http://localhost:2465/sign', [
            // set basic authorization
            'auth' => ['user', 'secret'],
            // set destination file
            'sink' => $dest,
            // defined multipart data
            'multipart' => [
                [
                    'name'     => 'thumbprint', // certificate thumbprint
                    'contents' => 'd33ac6cc6290bcfb4a23243af0ec98b4f49b2238'
                ],
                [
                    'name'     => 'position', // signature postion (x,y,width,height)
                    'contents' => '20,10,320,120'
                ],
                [
                    'name'     => 'file', // pdf
                    'contents' => fopen(__DIR__ . '/lorem-ipsum.pdf', 'r'),
                    'headers'  => ['Content-Type' => 'application/pdf']
                ]
            ]
        ]);
        // get response
        $this->assertEquals(is_file($dest), true);
    }
```



Read more about universis-signer [here](https://gitlab.com/universis/universis-signer.git).

