<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class SignerTest extends TestCase
{
    public function testHelloWorld(): void
    {
        $message = "Hello World!";
        $this->assertEquals($message, "Hello World!");
    }

    public function testGetCertificates(): void
    {
        $client = new Client();
        $response = $client->request('GET', 'http://localhost:2465/keystore/certs/', [
            // set basic authorization
            'auth' => ['user', 'secret'] 
        ]);
        // get certificate
        $certs = json_decode($response->getBody()) ;
        $this->assertNotNull($certs);
        var_dump($certs);
        $this->assertEquals($certs[0]->expired, false);
        $this->assertEquals($certs[0]->thumbprint, 'd33ac6cc6290bcfb4a23243af0ec98b4f49b2238');
    }

    public function testSignDocument(): void
    {
        $dest = __DIR__ . '/lorem-ipsum-signed.pdf';
        $client = new Client();
        $response = $client->request('POST', 'http://localhost:2465/sign', [
            // set basic authorization
            'auth' => ['user', 'secret'],
            // set destination file
            'sink' => $dest,
            // defined multipart data
            'multipart' => [
                [
                    'name'     => 'thumbprint', // certificate thumbprint
                    'contents' => 'd33ac6cc6290bcfb4a23243af0ec98b4f49b2238'
                ],
                [
                    'name'     => 'position', // signature postion (x,y,width,height)
                    'contents' => '20,10,320,120'
                ],
                [
                    'name'     => 'file', // pdf
                    'contents' => fopen(__DIR__ . '/lorem-ipsum.pdf', 'r'),
                    'headers'  => ['Content-Type' => 'application/pdf']
                ]
            ]
        ]);
        // get response
        $this->assertEquals(is_file($dest), true);
    }
}